package main

import (
	"./controller"
	"github.com/kataras/iris"
	"github.com/kataras/iris/context"
	"github.com/kataras/iris/mvc"
)

func main() {
	app := iris.New()

	app.RegisterView(iris.HTML("./templates", ".html").Layout("layouts/main_layout.html"))
	app.StaticWeb("/css", "./css")

	main := mvc.New(app)
	main.Handle(new(controller.HomeController))

	app.Run(iris.Addr(":8080"))
}

func showContactPage(context context.Context) {

}
