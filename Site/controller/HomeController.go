package controller

import (
	"github.com/kataras/iris/context"
)

type HomeController struct {
	Context context.Context
}

func (c *HomeController) Get() {
	showHomePage(c.Context)
}

func showHomePage(context context.Context) {
	context.ViewData("title", "WarWAOne - Home")
	context.View("view/home.html")
}
